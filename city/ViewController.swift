//
//  ViewController.swift
//  city
//
//  Created by Ahmed Adm on 16/08/2020.
//  Copyright © 2020 Ahmed Adm. All rights reserved.
//

import UIKit


struct districtServerResponse: Decodable {
    var id: Int!
    var districtName: String!
    var city: String!
    var lat: Double!
    var log: Double!
    var providers: String!
}

class ViewController: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate{
    
    var city_api     = "http://astalem.com/api/order/getcity"
    var district_api = "http://astalem.com/api/order/getdistrictbycity"
    
    @IBOutlet weak var cityFeild:UITextField!
    @IBOutlet weak var districtFeilds:UITextField!
    
    @IBOutlet var loading: UIActivityIndicatorView!
    @IBOutlet var loading_view: UIView!

    
    var cities    = [""]//Medina" , "Hail" , "Ryiadh" , "Dammam"]
    var districts = [""]
    
    var selectedCity = ""
    var selectedDistrict = ""
    var cityPickerView = UIPickerView()
    var districtPickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cityPickerView.delegate = self
        self.cityPickerView.dataSource = self
        self.cityPickerView.backgroundColor = UIColor.white
        
        self.districtPickerView.delegate = self
        self.districtPickerView.dataSource = self
        self.districtPickerView.backgroundColor = UIColor.white
        
        self.cityFeild.delegate      = self
        self.districtFeilds.delegate = self
        
        self.create_pickerView(sender: cityFeild)
        self.create_pickerView(sender: districtFeilds)
        
        self.get_cities_form_server()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismisskeyboard))
        self.view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
              

        
    }

    deinit{
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }

    func create_pickerView( sender:UITextField ){
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.backgroundColor = UIColor.white
        
        let doneButton   = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(done_pressed) )
        let space        = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector(cancel_pressed))
        toolbar.setItems([doneButton , space , cancelButton], animated: true)
        
        
        if sender.tag == 0 {
            self.cityFeild.inputAccessoryView = toolbar
            self.cityFeild.inputView = self.cityPickerView
        } else {
            self.districtFeilds.inputAccessoryView = toolbar
            self.districtFeilds.inputView = self.districtPickerView
        }
        
    }
    
    @objc func done_pressed(  ){
        
        DispatchQueue.main.async {
            self.cityFeild.text      = self.selectedCity
            self.districtFeilds.text = self.selectedDistrict
            
            self.cityFeild.resignFirstResponder()
            self.districtFeilds.resignFirstResponder()
            self.view.endEditing(true)
        }
        
        let indexOfDistrict = self.districtPickerView.selectedRow(inComponent: 0)
        let pickedDistrict  = self.districts[indexOfDistrict]
        
        print("selectedCity \(self.selectedCity) : pickedDistrict \(pickedDistrict)")
        print(self.selectedDistrict != pickedDistrict)
        
        
        // get district of city if he select a city
        if self.selectedCity.count > 0 &&  self.selectedDistrict.count == 0 {
            self.get_district_form_server(city: self.selectedCity)
        }
    }
    
    @objc func cancel_pressed(){
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.cityPickerView{
            return self.cities.count
        }else{
            return self.districts.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.districtPickerView {
            return self.districts[row]
        }else{
            return self.cities[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.cityPickerView{
            self.selectedDistrict = ""
            DispatchQueue.main.async {
                self.districtFeilds.text = ""
            }
            self.selectedCity = self.cities[row]
        }else{
            self.selectedDistrict = self.districts[row]
        }
    }

    
    // this function will connect to server to get the cities from it, then it will store data in array
    func get_cities_form_server(){
        
        let url = URL(string: self.city_api)
        
        DispatchQueue.main.async {
            self.loading.startAnimating()
            self.loading_view.isHidden = false
        }
        
        URLSession.shared.dataTask(with: url!){(data , response , error) in
            
            if data == nil{
                return
            }
            
            do{
                self.cities = try JSONDecoder().decode([String].self, from: data!)
                self.selectedCity = ""
                DispatchQueue.main.async {
                    self.cityFeild.text = ""
                }
                
            }catch{
                print("error in json \(error)")
            }
            
            DispatchQueue.main.async {
                self.loading.stopAnimating()
                self.loading_view.isHidden = true
            }
            
            }.resume()

    }
    
    // this function will get data from server to get districts by city name
    func get_district_form_server(city:String){
        
        let api = self.district_api + "?city=" + city
        let encoded_url = api.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: encoded_url!)
        
        DispatchQueue.main.async {
            self.loading.startAnimating()
            self.loading_view.isHidden = false
        }
        
        URLSession.shared.dataTask(with: url!){(data , response , error) in
            
            if data == nil{
                return
            }

//            let responseString = String(data: data!, encoding: .utf8)
//            print("responseString = \(responseString!)")

            self.districts.removeAll()
            
            do{
                
                var districtsResponse = try JSONDecoder().decode([districtServerResponse].self, from: data!)
                
                for onDistrict in districtsResponse{
                    self.districts.append(onDistrict.districtName!)
                }
                
            }catch{
                print("error in json \(error)")
            }
            
            DispatchQueue.main.async {
                self.loading.stopAnimating()
                self.loading_view.isHidden = true
            }
            
            }.resume()

    }
    
    // keyboard functions
    @objc func keyboardWillChange(notification : Notification){
        guard let keyboardRect = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification ||
            notification.name == UIResponder.keyboardWillChangeFrameNotification {
            view.frame.origin.y = -keyboardRect.height
        }else{
            view.frame.origin.y = 0
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.resignFirstResponder()
        self.view.endEditing(true)
        return false
    }
    
    @objc func dismisskeyboard(){
        self.view.endEditing(true)
    }
    

}

